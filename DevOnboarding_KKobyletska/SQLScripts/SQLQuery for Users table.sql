CREATE TABLE Users (
User_PK uniqueidentifier PRIMARY KEY DEFAULT NEWSEQUENTIALID(), 
FirstName nvarchar(50) NOT NULL,
SecondName nvarchar(50) NOT NULL,
Email nvarchar(256)
);