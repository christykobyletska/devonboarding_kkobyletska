CREATE TABLE UserInfo (
UserInfo_PK uniqueidentifier PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
User_PK uniqueidentifier FOREIGN KEY REFERENCES Users(User_PK),
Age int,
Phone int,
UserAddress nvarchar(256)
);